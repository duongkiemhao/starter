package com.siliconstack.training.adapter.demo_app.config


enum class ValuationStatus(val status: Int) {
    REQUEST(1),
}
