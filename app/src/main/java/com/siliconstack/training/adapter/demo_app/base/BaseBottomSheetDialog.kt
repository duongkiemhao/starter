package com.siliconstack.training.adapter.demo_app.base

import android.app.Dialog
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.siliconstack.training.R

abstract class BaseBottomSheetDialog:BottomSheetDialogFragment() {

    protected abstract fun init()
    protected abstract fun observe()
    protected abstract fun setListener()
    protected abstract fun load()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        observe()
        setListener()
        load()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener { dialog ->
            val d = dialog as BottomSheetDialog
            d.setCanceledOnTouchOutside(false)
            val bottomSheet: View? =
                d.findViewById<View>(R.id.design_bottom_sheet)
            bottomSheet?.let {
                BottomSheetBehavior.from(it).state =
                    BottomSheetBehavior.STATE_EXPANDED
                BottomSheetBehavior.from(it).isDraggable=true
            }
        }
        dialog.window?.attributes?.windowAnimations = R.style.BottomSheetDialogAnimation
        return dialog
    }
}