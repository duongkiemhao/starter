package com.siliconstack.training.adapter

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.setFragmentResult
import com.siliconstack.training.R
import com.siliconstack.training.databinding.FragmentSecondBinding
import kotlinx.android.synthetic.main.fragment_second.*

class SecondFragment : Fragment() {
//    private lateinit var communicator: Communicator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val binding:FragmentSecondBinding = DataBindingUtil.inflate(layoutInflater,R.layout.fragment_second,container,false)

        binding.btnSendData.setOnClickListener {
            val result = Bundle()
            val input = binding.etInput.text.toString()
            result.putString("data",input)
            setFragmentResult("requestKey", result)
            Log.i("TAG", "$result: ")
            }

        return binding.root
    }
}
