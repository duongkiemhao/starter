package com.siliconstack.training.adapter.demo_app.ui.welcome

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.siliconstack.training.R
import com.siliconstack.training.adapter.demo_app.base.BaseFragment
import com.siliconstack.training.databinding.WelcomeFragmentBinding

class WelcomeFragment: BaseFragment() {
    companion object {
        fun  newInstance(): WelcomeFragment {
            val fragment = WelcomeFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
    lateinit var binding: WelcomeFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.welcome_fragment, null, false)
        return binding.root
    }


    override fun init() {
    }

    override fun observe() {
    }

    override fun setListener() {
        binding.signBtn.setOnClickListener {
            findNavController().navigate(R.id.registerFragment)
        }
        binding.tvLogin.setOnClickListener {
            findNavController().navigate(R.id.loginFragment)
        }
    }

    override fun load() {
    }



}