package com.siliconstack.training.adapter

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.mvvm.retrofit_viewmodel.QuotesViewmodel
import com.siliconstack.training.databinding.FragmentFirstBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class FirstFragment : Fragment() {
    private val quotesViewModel: QuotesViewmodel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val binding: FragmentFirstBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.fragment_first_, container, false)

        //getting data retrofit call with koin
        quotesViewModel.quote.observe(viewLifecycleOwner, {
            Log.i("main", "onCreate:${it} ")
            binding.container.text = it.results.toString()
        })

        // getting data using fragment api
        binding.getData.setOnClickListener {
            val supportFragmentManager = activity?.supportFragmentManager
            supportFragmentManager?.setFragmentResultListener("requestKey",
                this) { requestKey, bundle ->
                var result = bundle.getString("data").toString()
                Log.i("TAG1", "$result: ")
                binding.tvReciever.text = result
            }
        }
  return binding.root
    }
}
