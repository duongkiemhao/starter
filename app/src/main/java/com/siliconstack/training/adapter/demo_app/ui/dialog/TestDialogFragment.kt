package com.siliconstack.training.adapter.demo_app.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.demo_app.base.BaseDialogFragment
import com.siliconstack.training.databinding.TestDialFragmentBinding

class TestDialogFragment:BaseDialogFragment() {

    companion object {
        fun  newInstance(): TestDialogFragment {
            val fragment = TestDialogFragment()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }

    lateinit var binding: TestDialFragmentBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.test_dial_fragment, null, false)
        return binding.root
    }
    override fun init() {

    }

    override fun observe() {

    }

    override fun setListener() {
        binding.btnCancel.setOnClickListener { dismiss() }
        binding.btnOk.setOnClickListener { dismiss() }
    }

    override fun load() {
    }
}