package com.siliconstack.training.adapter.mvvm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.adapter.mvvm.retrofit_viewmodel.QuotesViewmodel
import com.siliconstack.training.R
import com.siliconstack.training.databinding.ActivityRetrofitBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class RetrofitActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRetrofitBinding
    private val quotesViewModel: QuotesViewmodel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_retrofit)

        quotesViewModel.quote.observe(this,{
            Log.i("main", "onCreate:${it} ")
            binding.tvResult.text = it.results.toString()
        })

    }
}