package com.siliconstack.training.adapter.demo_app.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.demo_app.base.BaseBottomSheetDialog
import com.siliconstack.training.databinding.TestBtmSheetDialFragmentBinding

class TestBottomSheetDialogFragment:BaseBottomSheetDialog() {

    companion object {
        fun  newInstance(): TestBottomSheetDialogFragment {
            val fragment = TestBottomSheetDialogFragment()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }

    lateinit var binding: TestBtmSheetDialFragmentBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater,R.layout.test_btm_sheet_dial_fragment,null,false)
        return binding.root
    }
    override fun init() {

    }

    override fun observe() {

    }

    override fun setListener() {
        binding.btnSubmit.setOnClickListener { dismiss() }
    }

    override fun load() {

    }
}