package com.siliconstack.training.adapter.demo_app.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.list.listItems
import com.siliconstack.training.R
import com.siliconstack.training.adapter.demo_app.base.BaseFragment
import com.siliconstack.training.databinding.MaterialDialogFragmentBinding

class MaterialDialogFragment:BaseFragment() {

    companion object{
        fun newInstance():MaterialDialogFragment{
            val fragment = MaterialDialogFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var binding:MaterialDialogFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.material_dialog_fragment,null ,false)
        return binding.root
    }
    override fun init() {

    }

    override fun observe() {

    }

    override fun setListener() {
        binding.successDialog.setOnClickListener {
            showDialog()
        }
        binding.errorDialog.setOnClickListener {
        customDialog()
        }
    }

    override fun load() {
    }

     private fun showDialog(){
         val myItems = listOf("Hello", "World")
         activity?.let {
             val dialog = MaterialDialog(it)
                 .title(R.string.title)
                 .message(R.string.text)
                 .icon(R.drawable.dialog_icon)
                 .listItems(items = myItems)
                 .positiveButton(R.string.positive_btn){dialog->
                     Toast.makeText(context,"you clicked ok",Toast.LENGTH_SHORT).show()
                 }
                 .negativeButton(R.string.negative_btn){
                     Toast.makeText(context,"you clicked cancel",Toast.LENGTH_SHORT).show()
                 }



             dialog.show()
         }
    }

    private fun customDialog(){
        activity?.let {
            val dialog = MaterialDialog(it)
                .positiveButton(R.string.positive_btn){
                    Toast.makeText(context,"you clicked ok",Toast.LENGTH_SHORT).show()
                }
                .negativeButton(R.string.negative_btn)
                .customView(R.layout.filter_custom_dialog)
            dialog.show()
        }

    }
}