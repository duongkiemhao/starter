package com.siliconstack.training.adapter.demo_app.ui.login_signup

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.siliconstack.training.R
import com.siliconstack.training.adapter.demo_app.base.BaseFragment

import com.siliconstack.training.adapter.demo_app.ui.home.HomeActivity
import com.siliconstack.training.adapter.demo_app.ui.login_signup.model.User
import com.siliconstack.training.databinding.RegisterFragmentBinding

class RegisterFragment: BaseFragment() {
    companion object {
        fun  newInstance(): RegisterFragment {
            val fragment = RegisterFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
    lateinit var email: String
    lateinit var password:String
    lateinit var username:String
    lateinit var profileuserImage:Uri
    lateinit var userImage:String
     lateinit var mAuth: FirebaseAuth
    lateinit var UserProfileImage: ActivityResultLauncher<Intent>

     lateinit var database: DatabaseReference
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.register_fragment, null, false)



        return binding.root


    }
    lateinit var binding: RegisterFragmentBinding
    override fun init() {
        UserProfileImage = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result: ActivityResult ->
            if (result?.resultCode == Activity.RESULT_OK){
                profileuserImage = (result.data?.data!!)
                Log.d("Debug", "onCreate:$profileuserImage ")
//                binding.profileUserImg.setImageURI(uri)
                Glide.with(requireContext())
                    .load(profileuserImage).
                    diskCacheStrategy(DiskCacheStrategy.ALL).into(binding.registerProfileImage)
            }
            else{
                Log.d("Debug", "image Not recived ")
                Toast.makeText(context,"Failed to choose image.",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun observe() {
    }

    override fun setListener() {
        binding.registerProfileImage.setOnClickListener{
            UserProfileImage.launch(Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI))
        }
        binding.signUpBtn.setOnClickListener {
            activity?.let {
                 email = binding.emailEditText.text.toString()
                 password = binding.passwordEditText.text.toString()
                username = binding.usernameEditText.text.toString()
                userImage = profileuserImage.toString()
//                val userImage = profileuserImage
                Log.d("Debug", "regestring User ")
                registerUser(email,password)
                Log.d("Debug", "creating user database: ")
                createUserToDataBase(username,email,password,userImage)
                goToHome()
                goNext()
            }
        }

        binding.backLogin.setOnClickListener{ goToLoginFragment() }

        binding.toLoginFragment.setOnClickListener { goToLoginFragment() }


    }



    private fun registerUser(email:String,password:String) {
        mAuth = FirebaseAuth.getInstance()
        mAuth.createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener() { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Debug", "$email:+ $password + ${task.result?.user?.uid}")
                }
            }
    }

    private fun createUserToDataBase(username:String,email:String,password: String, userImage:String) {
            database = FirebaseDatabase.getInstance().getReference("User")
            val user = User(username,email,password,userImage)
        database.child(username).setValue(user).addOnSuccessListener {
            Toast.makeText(context,"User uploaded to Firebase",Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(context,"Failed to Upload Data",Toast.LENGTH_SHORT).show()
        }
    }
    private fun goToHome() {
        val intent = Intent(context,HomeActivity()::class.java)
        context?.startActivity(intent)
    }
    private fun goToLoginFragment() {
        findNavController().navigate(R.id.loginFragment)
    }

    override fun load() {}

}