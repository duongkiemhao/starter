package com.siliconstack.training.adapter.examples

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseActivity

import com.siliconstack.training.adapter.examples.share_pref.SharedPreferencesActivity
import com.siliconstack.training.databinding.ActivityIntentBinding

class IntentActivity: BaseActivity() {

    lateinit var binding:ActivityIntentBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_intent)
        onCreatedViewDone()
    }
    override fun init() {

    }

    override fun observe() {

    }

    override fun setListener() {
       binding.toAnimationByCode.setOnClickListener {
           val intent = Intent(this, AnimateActivity::class.java)
           startActivity(intent)
           goNext()
       }
        binding.toMotionActivity.setOnClickListener {
            val intent = Intent(this, MotionActivity::class.java)
            startActivity(intent)
            goNext()
        }
        binding.toSharedPreferences.setOnClickListener {
            val intent = Intent(this, SharedPreferencesActivity::class.java)
            startActivity(intent)
            goNext()
        }
    }

    override fun load() {
        
        val MyJson:String = """
            {
            "name":"khan"
            }
        """

        }

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }


