package com.siliconstack.training.adapter.mvvm.data

data class Quotes(
    val results: List<Result>
)