package com.example.covidtracker.covid.selection

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseBottomSheetDialogFragment
import com.siliconstack.training.adapter.base.base.BaseCodeName
import com.siliconstack.training.adapter.base.config.Config
import com.siliconstack.training.databinding.SelectionFragmentBinding
import com.siliconstack.training.databinding.SelectionFragmentRvItemBinding

class SelectionFragment: BaseBottomSheetDialogFragment() {

    companion object {
        fun <T : BaseCodeName> newInstance(
            item: ArrayList<T>, title: String, selectedCode: String? = null,
            showSearchBar: Boolean = false, autoCompleteItemListner: AutoCompleteItemListener
        ): SelectionFragment {
            val fragment = SelectionFragment()
            val args = Bundle()
            args.putParcelableArrayList(Config.ITEMS, item)
            args.putString(Config.TITLE, title)
            args.putString(Config.SELECTED_CODE, selectedCode)
            args.putBoolean(Config.SHOW_SEARCH_BAR, showSearchBar)
            args.putParcelable(Config.LISTENER, autoCompleteItemListner)
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var binding: SelectionFragmentBinding

    private var autoCompleteDisplayListener:AutoCompleteDisplayListener?=null

    private val item by lazy {
        arguments?.getParcelableArrayList<BaseCodeName>(Config.ITEMS) as ArrayList<BaseCodeName>
    }

    private val title by lazy {
        arguments?.getString(Config.TITLE)
    }

    private val selectedCode by lazy {
        arguments?.getString(Config.SELECTED_CODE)
    }

    private val showSearchBar by lazy {
        arguments?.getBoolean(Config.SHOW_SEARCH_BAR)
    }

    private val autoCompleteItemListner: AutoCompleteItemListener by lazy {
        arguments?.getParcelable(Config.LISTENER)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.selection_fragment, null, false)
        return binding.root
    }


    override fun init() {
        binding.txtTitle.text = title
        binding.selectionRecyclerView.apply {
            val selectedIndex = item.indexOfFirst { it.getCode().equals(selectedCode, true) }
            adapter = SelectionFragmentAdapter(item, selectedIndex)
            layoutManager = LinearLayoutManager(context)
            val divider = DividerItemDecoration(context, RecyclerView.VERTICAL)
            divider.setDrawable(resources.getDrawable(R.drawable.list_divider_eaecef))
            addItemDecoration(divider)
            scrollToPosition(selectedIndex)
        }
    }

    override fun observe() {

    }

    override fun setListener() {
        binding.btnClose.setOnClickListener {
            dismiss()
        }
    }

    override fun load() {

    }

    inner class SelectionFragmentAdapter(var items:List<BaseCodeName>, private val selectedIndex: Int):
            RecyclerView.Adapter<SelectionFragmentAdapter.ViewHolder>(),AutoCompleteFilterListner{

        private val originItem = (items as ArrayList).clone() as ArrayList<BaseCodeName>


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val binding = DataBindingUtil.inflate<SelectionFragmentRvItemBinding>(LayoutInflater.from(parent.context),R.layout.selection_fragment_rv_item,parent,false)
            return ViewHolder(binding)
        }
        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                val item:BaseCodeName = items[position]
           holder.binding.textView.text =if(autoCompleteDisplayListener==null) item.getDesc() else autoCompleteDisplayListener!!.getName(item)
            holder.itemView.setOnClickListener {
                items.forEach{it.isSelect = false}
                item.isSelect = true
                autoCompleteItemListner.onClick(holder.adapterPosition,items)
                dismiss()
            }
            holder.binding.textView.isChecked = holder.adapterPosition==selectedIndex
        }

        override fun getItemCount(): Int {
           return items.size
        }

        inner class ViewHolder( val binding: SelectionFragmentRvItemBinding):RecyclerView.ViewHolder(binding.root)


        override fun filter(text: String) {

        }
    }
}

interface AutoCompleteDisplayListener {
        fun getName(name:BaseCodeName):String
}
interface AutoCompleteFilterListner{
    fun filter(text:String)
}

interface AutoCompleteItemListener : Parcelable {
    fun <T:BaseCodeName> onClick(position: Int, item: List<T>)
    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }
    override fun describeContents(): Int {
        return 0
    }
}


