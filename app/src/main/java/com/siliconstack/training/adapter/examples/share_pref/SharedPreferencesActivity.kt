package com.siliconstack.training.adapter.examples.share_pref

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseActivity
import com.siliconstack.training.databinding.SharePreferencesActivityBinding



class SharedPreferencesActivity: BaseActivity() {

    lateinit var binding:SharePreferencesActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.share_preferences_activity)
        onCreatedViewDone()
    }
    override fun init() {
        supportFragmentManager.beginTransaction().add(R.id.nav_host_share_pref,SharePrefFragmentOne.newInstance()
        ).addToBackStack(SharePrefFragmentOne::class.simpleName).commit()
    }

    override fun observe() {

    }

    override fun setListener() {
    }

    override fun load() {

    }
}