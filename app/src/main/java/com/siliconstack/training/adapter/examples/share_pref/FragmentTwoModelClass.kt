package com.siliconstack.training.adapter.examples.share_pref

data class FragmentTwoModelClass(
    val CustomerName:String,
    val CustomerAmount:String,
    val switch:Boolean?,
    val togDiscount:Boolean?,
    val checkSwitch:Boolean?,
    val changeSwitch:Boolean?
)