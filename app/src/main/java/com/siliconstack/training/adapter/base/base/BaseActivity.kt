package com.siliconstack.training.adapter.base.base

import androidx.appcompat.app.AppCompatActivity
import com.siliconstack.training.R


abstract class BaseActivity : AppCompatActivity() {



    protected abstract fun init()
    protected abstract fun observe()
    protected abstract fun setListener()
    protected abstract fun load()

    protected fun onCreatedViewDone(
//        loadError: MutableLiveData<String?>? = null,
//        loading: MutableLiveData<Boolean>? = null
    ) {

        init()
        observe()
        setListener()
    }


    fun goBack() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    fun goNext() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }

}
