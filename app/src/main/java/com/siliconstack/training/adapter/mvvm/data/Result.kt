package com.siliconstack.training.adapter.mvvm.data

data class Result(
    val content: String
)