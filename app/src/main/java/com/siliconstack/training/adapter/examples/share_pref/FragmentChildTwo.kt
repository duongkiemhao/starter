package com.siliconstack.training.adapter.examples.share_pref

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.setFragmentResultListener
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseFragment
import com.siliconstack.training.databinding.ChildOneFragmentBinding
import com.siliconstack.training.databinding.ChildTwoFragmentBinding

class FragmentChildTwo:BaseFragment() {

    lateinit var binding: ChildTwoFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.child_two_fragment,null,false)
        return binding.root
    }
    override fun init() {
        Log.i(TAG, "init: listner")
        childFragmentManager.setFragmentResultListener(SharePrefFragmentOne::class.java.simpleName,requireActivity()) { _, bundle ->
            val item=bundle.getString(TAG)
            Log.i(TAG, "init: recieved")
            Log.i("Debug", "setListener in init:${item.toString()}")
        }
    }

    override fun observe() {

    }

    override fun setListener() {

    }

    override fun load() {

    }
}