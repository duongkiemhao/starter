package com.siliconstack.training.adapter.examples.share_pref

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseFragment
import com.siliconstack.training.databinding.SharePrefFragmentTwoBinding

class SharePrefFragmentTwo:BaseFragment() {
    companion object {
        fun  newInstance(): SharePrefFragmentTwo {
            val fragment = SharePrefFragmentTwo()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }
//    lateinit var textOne:String
    private var isCheckedSave:Boolean ? = null
    private var togDiscountSwitch:Boolean ? = null
    private var testSwitch:Boolean ? = null
    private var changeSwitch:Boolean ? = null

//    lateinit var textTwo:String
//     var mExampleList = ArrayList<FragmentTwoModelClass>
    lateinit var itemsCustomer: FragmentTwoModelClass
    lateinit var binding: SharePrefFragmentTwoBinding

    private val customerDetail by lazy {
        PreferenceHelper.getCustomerDetail(requireContext(),"details") as FragmentTwoModelClass
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.share_pref_fragment_two,null,false)
        return binding.root
    }

    override fun init() {
        Log.i(TAG, "Test switch init: $testSwitch")
        Log.i(TAG, "change switch init: $changeSwitch")
        val step :Int = 2
        when (step){
            1 -> {
                Log.i(TAG, "step one: ")
            }
            2 -> {
                Log.i(TAG, "step two")
            }
        }
    }

    override fun observe() {

    }

    override fun setListener() {
        binding.modelSwitch.setOnCheckedChangeListener { _, isChecked ->
             isCheckedSave = isChecked
        }
        binding.btnNext.setOnClickListener {
            parentFragmentManager.beginTransaction().add(R.id.nav_host_share_pref,SharePrefFragmentThree.newInstance())
                .addToBackStack(SharePrefFragmentThree::class.simpleName).commit()
        }
        binding.btnBack.setOnClickListener {
            parentFragmentManager.popBackStack()
        }
        binding.discountSwitch.setOnCheckedChangeListener {_, isChecked ->
            togDiscountSwitch = isChecked
            Log.i(TAG, "discountSwitch: $togDiscountSwitch")
        }
        binding.textSwitch.setOnCheckedChangeListener {_, isChecked ->
            testSwitch = isChecked
            Log.i(TAG, "checkSwitch: $testSwitch")
        }
        binding.textSwitch.setOnCheckedChangeListener {_, isChecked ->
            changeSwitch = isChecked
            Log.i(TAG, "checkSwitch: $changeSwitch")
        }
        binding.save.setOnClickListener {
            val textOne = binding.editCustomerName.text.toString()
            Log.i("Debug", "setListener: $textOne")
            val textTwo = binding.editAmount.text.toString()
            itemsCustomer = FragmentTwoModelClass(textOne,textTwo,isCheckedSave,togDiscountSwitch,testSwitch,changeSwitch)
            Log.i("Debug", "items:${itemsCustomer.CustomerAmount} ")
            Log.i("Debug", "items:${itemsCustomer.CustomerName} ")
            PreferenceHelper.setCustomerDetail(requireContext(),"details",itemsCustomer)
        }
        binding.load.setOnClickListener {

        }
    }
    override fun load() {
//        val customerDetail =  PreferenceHelper.getCustomerDetail(requireContext(),"details")
        binding.tvCustomerName.text = customerDetail.CustomerName
        binding.tvTotalAmount.text = customerDetail.CustomerAmount
        binding.modelSwitch.isChecked = customerDetail.switch?:false
        binding.discountSwitch.isChecked = customerDetail.togDiscount?: false
        binding.textSwitch.isChecked = customerDetail.checkSwitch?: false
        binding.changeSwitch.isChecked = customerDetail.changeSwitch?: false
    }

}