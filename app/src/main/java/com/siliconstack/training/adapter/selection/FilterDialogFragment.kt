package com.siliconstack.training.adapter.selection

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.example.covidtracker.covid.selection.AutoCompleteItemListener
import com.example.covidtracker.covid.selection.SelectionFragment
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseCodeName
import com.siliconstack.training.adapter.base.base.BaseDialogFragment
import com.siliconstack.training.adapter.base.base.CodeName

import com.siliconstack.training.databinding.FilterDialogFragmentsBinding

class FilterDialogFragment: BaseDialogFragment() {

    companion object {
        fun newInstance(): FilterDialogFragment {
            val fragment = FilterDialogFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var binding: FilterDialogFragmentsBinding
    private var selectedLetter: CodeName?=null

    private val letters by lazy {
        arrayListOf(CodeName(id="f",name = "fruits"),
            CodeName(id="a",name = "apple"),
            CodeName(id="b",name = "banana"),
            CodeName(id="c",name = "cat"),
            CodeName(id="d",name = "dog"),
            CodeName(id="e",name = "ester"))
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.filter_dialog_fragments, null, false)
        return binding.root
    }

    override fun init() {

    }

    override fun observe() {

    }

    override fun setListener() {
        binding.txtClearFilter.setOnClickListener {
            dismiss()
            Log.d("Debug", "clear:clicked ")
        }
        binding.letterSelector.editText?.setOnFocusChangeListener { v, hasFocus ->
            Log.d("Debug", "setListener:Clicked ")
                if(hasFocus) {
                    SelectionFragment.newInstance(
                        letters,
                        title = "Letters",
                        showSearchBar = false,
                        selectedCode = selectedLetter?.getCode(),
                        autoCompleteItemListner = object : AutoCompleteItemListener {
                            override fun <T : BaseCodeName> onClick(position: Int, item: List<T>) {
                                selectedLetter = item[position] as CodeName
                                binding.letterSelector.editText?.setText(selectedLetter?.name)
                            }
                        }).show(parentFragmentManager,null)
                }
            }
        }


    override fun load() {
        val word = listOf("apple","and","application", "banana", "blackberries", "blueberries", "cherries", "grapes", "lemon",
            "orange", "peaches", "pear", "pineapple", "plums", "raspberries", "strawberries", "watermelon")
        (0 until word.count()).forEach { i ->
            val textView = LayoutInflater.from(context).inflate(
                R.layout.word_list_item,binding.flowWordWrapper,false
            ).apply {
                this as TextView
                id = View.generateViewId()
                text = word[i]
            }
            binding.flowWordWrapper.addView(textView)
            binding.flowWords.addView(textView)
        }
    }
}