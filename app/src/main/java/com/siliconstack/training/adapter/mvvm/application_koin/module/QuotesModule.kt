package com.siliconstack.training.adapter.mvvm.application_koin.module

import com.siliconstack.training.adapter.mvvm.viewmodel_repository.QuotesRepository
import com.siliconstack.training.adapter.mvvm.retrofit_viewmodel.QuotesViewmodel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val QuotesViewModelModule = module {
    viewModel { QuotesViewmodel(get()) }
}
val QuotesRepositoryModule = module{
    factory { QuotesRepository(get()) }
}