package com.siliconstack.training.adapter.mvvm.application_koin

import android.app.Application
import com.siliconstack.training.adapter.mvvm.application_koin.module.QuotesRepositoryModule
import com.siliconstack.training.adapter.mvvm.application_koin.module.QuotesViewModelModule
import com.siliconstack.training.adapter.mvvm.application_koin.module.retrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BaseApplication: Application() {
    companion object {
         var instance: BaseApplication = BaseApplication()
    }
    override fun onCreate() {
        super.onCreate()
        instance = this
        startKoin {
            androidLogger()
            androidContext(this@BaseApplication)
            modules(listOf(
                retrofitModule, QuotesViewModelModule, QuotesRepositoryModule
            ))
        }
    }
}