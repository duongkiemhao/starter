package com.siliconstack.training.adapter.demo_app.config

class Config {
    companion object{
        const val X_APP_ID="demo_app"
        const val SPLASH_ANIMATION_DURATION=3000L
        const val ANIMATION_DURATION=500L
        const val NETWORK_TIMEOUT :Long = 30*60*1000L
        const val TAG="---demo_debug---"
        const val DEVICE_TYPE="Android"
        const val PAGING_LIMIT=10
        const val IMAGE_REQUEST_CODE = 1

        //selection fragment constant
        const val ITEMS="items"
        const val TITLE="title"
        const val SELECTED_CODE="selectedCode"
        const val SHOW_SEARCH_BAR="showSearchBar"
        const val LISTENER="autoCompleteItemListener"

    }
}