package com.siliconstack.training.adapter.base.base

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class CodeName(var id:String?,var name:String?,var imageUrl:String?=null): BaseCodeName() {
    constructor():this(null,null)

    override fun getCode(): String? {
        return id
    }

    override fun getDesc(): String? {
        return name
    }
}


abstract class BaseCodeName(var isSelect:Boolean=false) : Parcelable {
    abstract fun getCode():String?
    abstract fun getDesc():String?
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (isSelect) 1 else 0)
    }
    override fun describeContents(): Int {
        return 0
    }
}