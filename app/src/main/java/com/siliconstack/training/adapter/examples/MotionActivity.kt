package com.siliconstack.training.adapter.examples

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseActivity
import com.siliconstack.training.databinding.MotionLayoutActivityBinding

class MotionActivity: BaseActivity() {

    lateinit var binding:MotionLayoutActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.motion_layout_activity)
    }
    override fun init() {

    }

    override fun observe() {

    }

    override fun setListener() {

    }

    override fun load() {

    }
}