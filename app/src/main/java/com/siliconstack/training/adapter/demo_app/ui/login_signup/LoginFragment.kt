package com.siliconstack.training.adapter.demo_app.ui.login_signup

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.siliconstack.training.R
import com.siliconstack.training.adapter.demo_app.base.BaseFragment
import com.siliconstack.training.adapter.demo_app.ui.home.HomeActivity
import com.siliconstack.training.databinding.LoginFragmentBinding


class LoginFragment:BaseFragment() {

    companion object {
        fun  newInstance(): LoginFragment {
            val fragment = LoginFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, null, false)
        return binding.root
    }

    lateinit var binding: LoginFragmentBinding
    override fun init() {

    }

    override fun observe() {
    }

    override fun setListener() {
        binding.loginBtn.setOnClickListener {
            // both method to navigate from fragment to activity
//         val intent = Intent(this@LoginFragment.requireContext(), HomeActivity()::class.java)
//            startActivity(intent)
//            goNext()
            activity?.let{
                val intent = Intent(it, HomeActivity::class.java)
                it.startActivity(intent)
                goNext()
            }
//            startActivity<HomeActivity>()    // anko library
        }


        binding.toSignupFragment.setOnClickListener {
            findNavController().navigate(R.id.registerFragment)
        }
    }

    override fun load() {
    }
}