package com.siliconstack.training.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.siliconstack.training.R

class AdapterRecyclerView(private val listViewType: List<Int>) : RecyclerView.Adapter<AdapterRecyclerView.ViewHolder>() {

    companion object {
        const val layout_a = 1
        const val layout_b = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            layout_a -> ViewHolderItemA(inflater.inflate(R.layout.layout_item_a, null))
            else -> ViewHolderItemB(inflater.inflate(R.layout.layout_item_b,null))
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewType = listViewType[position]
    }

    override fun getItemCount(): Int = listViewType.size

    override fun getItemViewType(position: Int): Int = listViewType[position]

    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class ViewHolderItemA(itemView: View) : ViewHolder(itemView) {
        val textView: TextView = itemView.findViewById(R.id.text_view)
    }

    inner class ViewHolderItemB(itemView: View) : ViewHolder(itemView)

}