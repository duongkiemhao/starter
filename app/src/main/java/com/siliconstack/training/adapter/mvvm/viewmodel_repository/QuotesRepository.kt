package com.siliconstack.training.adapter.mvvm.viewmodel_repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.siliconstack.training.adapter.mvvm.data.Quotes
import com.siliconstack.training.adapter.mvvm.utils.QuoteService




class QuotesRepository(private val quoteService: QuoteService) {

    private val quoteslivedata = MutableLiveData<Quotes>()

    val quotes:LiveData<Quotes>
    get() = quoteslivedata

    suspend fun getQuote (){
        val result = quoteService.getQuote()
        if (result != null){
//            Log.i("main", "getQuote: ${result.body()}")
            quoteslivedata.postValue(result)
            Log.i("quoteRepo", "getQuote:${result} ")
        }
    }
}