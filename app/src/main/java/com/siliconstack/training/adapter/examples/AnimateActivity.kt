package com.siliconstack.training.adapter.examples

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseActivity
import com.siliconstack.training.databinding.AnimateActivityBinding

class AnimateActivity: BaseActivity() {

    lateinit var binding:AnimateActivityBinding
    private var isAnimClicked = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.animate_activity)
        onCreatedViewDone()
    }
    override fun init() {

    }

    override fun observe() {

    }

    override fun setListener() {

        binding.startAnim.setOnClickListener {
            binding.cakeImage.setImageResource(R.drawable.ic_anim_cake)
            Log.i("Debug", "setListener: Clicked")
        startAnim()
        }


    }

    override fun load() {

    }
    private fun startAnim(){
        isAnimClicked = !isAnimClicked
        if (isAnimClicked){
            binding.cakeImage.let {
                AnimateCake.animCake(it)
            }
        }else{
            AnimateCake.init(binding.cakeImage)
        }
    }
}
class AnimateCake(){
    companion object{
         fun animCake(v:View){
            v.animate().apply {
                duration = 2000
                translationY(350f)
                scaleX(10f)
                scaleY(10f)
                alpha(0.5f)
                alpha(1f)
                v.visibility = View.VISIBLE
            }.start()
        }

        fun init(v:View){
            v.visibility = View.GONE
            Log.i("Debug", "init: visibility gone")

        }
    }
}