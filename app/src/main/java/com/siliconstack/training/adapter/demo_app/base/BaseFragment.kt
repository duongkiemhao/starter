package com.siliconstack.training.adapter.demo_app.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.siliconstack.training.R


abstract class BaseFragment:Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        observe()
        setListener()
        load()
    }

    protected abstract fun init()
    protected abstract fun observe()
    protected abstract fun setListener()
    protected abstract fun load()

    fun goBack() {
        activity?.overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    fun goNext() {
        activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }



}