package com.siliconstack.training.adapter

interface Communicator {

    fun passData(editTexInput : String)
}