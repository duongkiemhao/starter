package com.siliconstack.training.adapter.mvvm.utils

import com.siliconstack.training.adapter.mvvm.data.Quotes
import retrofit2.http.GET

interface QuoteService {

    @GET("/quotes")
    suspend fun getQuote(): Quotes

}