package com.siliconstack.training.adapter.demo_app.ui.login_signup.model

data class User(
    val username:String,
    val email:String,
    val password:String,
    val userImage:String
)