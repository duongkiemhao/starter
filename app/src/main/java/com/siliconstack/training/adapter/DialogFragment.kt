package com.siliconstack.training.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.siliconstack.training.R
import com.siliconstack.training.databinding.FragmentDialogBinding
import kotlinx.android.synthetic.main.fragment_dialog.*
import kotlinx.android.synthetic.main.fragment_dialog.view.*


class DialogFragment : DialogFragment() {
    private lateinit var binding:FragmentDialogBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(layoutInflater,R.layout.fragment_dialog,container,false)

        sentData()
        return binding.root
    }

    private fun sentData() {
        // data sharing using the fragment api
        binding.btnSendDataFrag1.setOnClickListener {
            val bundle = Bundle()
            val input_text = binding.etDialog.text.toString()
            bundle.putString("Data", input_text)
            setFragmentResult("DataKey", bundle)

        }
    }
}