package com.siliconstack.training.adapter.selection

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseActivity
import com.siliconstack.training.databinding.HostActivityBinding

class MainHostActivity : BaseActivity(){

    lateinit var binding: HostActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.host_activity)
        super.onCreatedViewDone()
    }

    override fun init() {
        supportFragmentManager.beginTransaction().add(R.id.activity_host_nav_fragment, MainFragment.newInstance())
            .addToBackStack(MainFragment::class.java.simpleName).commit()
    }

    override fun observe() {

    }

    override fun setListener() {
    }

    override fun load() {
        
    }
}