package com.siliconstack.training.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.siliconstack.training.R


class RecyclerViewFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_recyclerview, container, false)

        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val listViewType = mutableListOf(
            AdapterRecyclerView.layout_a,AdapterRecyclerView.layout_b,
            AdapterRecyclerView.layout_b,AdapterRecyclerView.layout_a,
            AdapterRecyclerView.layout_b,AdapterRecyclerView.layout_a,
            AdapterRecyclerView.layout_b,AdapterRecyclerView.layout_a,
            AdapterRecyclerView.layout_b,AdapterRecyclerView.layout_a,
            AdapterRecyclerView.layout_b,AdapterRecyclerView.layout_a,
            AdapterRecyclerView.layout_b,AdapterRecyclerView.layout_a,
            AdapterRecyclerView.layout_b,AdapterRecyclerView.layout_a, )

        val adapterRecyclerView = AdapterRecyclerView(listViewType = listViewType)
        recyclerView.adapter = adapterRecyclerView

        return view
    }
}