package com.siliconstack.training.adapter.selection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseFragment
import com.siliconstack.training.databinding.MainFragmentBinding


class MainFragment: BaseFragment() {
    companion object{
        fun newInstance(): MainFragment {
            val fragment = MainFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment,null,false)
        return binding.root
    }

    override fun init() {

    }

    override fun observe() {

    }

    override fun setListener() {
        binding.txtFilter.setOnClickListener {
            FilterDialogFragment.newInstance().show(parentFragmentManager,null)
        }
    }

    override fun load() {
    }


}