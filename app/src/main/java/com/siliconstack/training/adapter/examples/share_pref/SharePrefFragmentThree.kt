package com.siliconstack.training.adapter.examples.share_pref

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseFragment
import com.siliconstack.training.databinding.SharePrefFragmentThreeBinding

class SharePrefFragmentThree:BaseFragment() {
    companion object {
        fun  newInstance(): SharePrefFragmentThree {
            val fragment = SharePrefFragmentThree()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }

    lateinit var binding:SharePrefFragmentThreeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.share_pref_fragment_three,null,false)
        return binding.root
    }
    override fun init() {
        parentFragmentManager.beginTransaction()
            .add(R.id.nav_host_share_pref, FragmentChildOne())
            .addToBackStack(FragmentChildOne::class.simpleName).commit()
    }


    override fun observe() {

    }

    override fun setListener() {

    }

    override fun load() {

    }
}