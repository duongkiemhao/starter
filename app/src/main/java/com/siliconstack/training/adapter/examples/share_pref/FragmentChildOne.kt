package com.siliconstack.training.adapter.examples.share_pref

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.setFragmentResult
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseFragment
import com.siliconstack.training.databinding.ChildOneFragmentBinding

class FragmentChildOne:BaseFragment() {

    lateinit var binding:ChildOneFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.child_one_fragment,null,false)
        return binding.root
    }
    override fun init() {
        val textFromOne = PreferenceHelper.getText(requireContext(),MATERIAL)
        binding.tvChildOne.text = textFromOne.toString()
    }

    override fun observe() {

    }

    override fun setListener() {
        binding.sendTwo.setOnClickListener {
            val result = Bundle()
            val etTest = binding.etToTwo.text.toString()
            result.putString("data",etTest)
            parentFragmentManager.setFragmentResult("requestKey", result)

        }
        binding.childOneImgNext.setOnClickListener {
//            parentFragmentManager.beginTransaction()
//                .add(R.id.nav_host_share_pref, FragmentChildTwo())
//                .addToBackStack(FragmentChildTwo::class.simpleName).commit()
            parentFragmentManager.beginTransaction()
                .add(R.id.nav_host_share_pref, SharePrefFragmentTwo.newInstance())
                .addToBackStack(SharePrefFragmentTwo::class.simpleName).commit()
        }

    }

    override fun load() {

    }
}