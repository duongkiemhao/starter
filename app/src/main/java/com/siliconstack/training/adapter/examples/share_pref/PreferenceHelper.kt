package com.siliconstack.training.adapter.examples.share_pref

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.siliconstack.training.adapter.mvvm.application_koin.BaseApplication


class PreferenceHelper() {
    companion object{
        const val Key = "Primary_key"
        private fun getDefault(context: Context): SharedPreferences {
//            val sharePrefKey =  BaseApplication.instance
//                .packageName
            return context.getSharedPreferences("SharePref",Context.MODE_PRIVATE)
        }
        fun putText(context: Context, key:String, string: String){
            getDefault(context).edit().putString(key,string).apply()
        }

        fun getText(context: Context, key:String):String?{
            return getDefault(context).getString(key, null)
        }

        fun putCheckedText(context: Context,key: String,check: Boolean){
            getDefault(context).edit().putBoolean(key,check)
        }
        fun getCheckedText(context: Context,key:String):Boolean{
            return getDefault(context).getBoolean(key,false)
        }
        fun setCustomerDetail(context: Context,key: String,items:FragmentTwoModelClass){
            getDefault(context).edit().putString(key, Gson().toJson(items)).apply()
        }
        fun getCustomerDetail(context: Context,key: String):FragmentTwoModelClass{
            return Gson().fromJson(getDefault(context).getString(key, ""), object : TypeToken<FragmentTwoModelClass>() {}.type)
        }
        fun setSwitchToggleBtn(context: Context,key: String,isChecked:Boolean){
            getDefault(context).edit().putBoolean(key,isChecked).apply()
        }
        fun getSwitchToggleBtn(context: Context,key: String):Boolean{
            return getDefault(context).getBoolean(key, false)
        }

        fun setFragmentTwoSwitch(context: Context,check: Boolean){
            getDefault(context).edit().putBoolean("switch",check).apply()
        }
        fun getFragmentTwoSwitch(context: Context):Boolean{
            return getDefault(context).getBoolean("switch",false)
        }


    }
}