package com.siliconstack.training.adapter.demo_app

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.demo_app.ui.dialog.ModelBottomSheet
import com.siliconstack.training.adapter.demo_app.ui.dialog.TestBottomSheetDialogFragment
import com.siliconstack.training.adapter.demo_app.ui.dialog.TestDialogFragment
import com.siliconstack.training.databinding.MainActivityBinding


class MainActivity:AppCompatActivity(){

    lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity)
        setListener()
    }


     fun setListener() {
        binding.dfBtn.setOnClickListener {
            Log.i("dfbtn", "listner is on ")
            TestDialogFragment.newInstance().show(supportFragmentManager,"")
        }
         binding.btmSheetBtn.setOnClickListener {
             TestBottomSheetDialogFragment.newInstance().show(supportFragmentManager,"")
         }
         binding.modelBottomSheet.setOnClickListener {
             ModelBottomSheet.newInstance().show(supportFragmentManager,"")
         }

         }
    }



