package com.siliconstack.training.adapter.movie_retrofit.movie_respository


import com.siliconstack.training.adapter.movie_retrofit.api_service.RetrofitService

class MovieRepository (private val retrofitService: RetrofitService) {

     fun getAllMovies() = retrofitService.getAllMovies()
}