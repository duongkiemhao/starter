package com.siliconstack.training.adapter.demo_app.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseFragment
import com.siliconstack.training.databinding.FragmentAddBlogPostBinding
import com.siliconstack.training.databinding.LoginFragmentBinding

class FragmentAddBlogPost: BaseFragment() {

    companion object {
        fun  newInstance(): FragmentAddBlogPost {
            val fragment = FragmentAddBlogPost()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_blog_post, null, false)
        return binding.root
    }

    lateinit var binding: FragmentAddBlogPostBinding

    override fun init() {

    }

    override fun observe() {

    }

    override fun setListener() {

    }

    override fun load() {

    }
}