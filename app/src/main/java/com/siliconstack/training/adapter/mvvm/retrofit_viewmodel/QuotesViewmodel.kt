package com.siliconstack.training.adapter.mvvm.retrofit_viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.siliconstack.training.adapter.mvvm.data.Quotes
import com.siliconstack.training.adapter.mvvm.viewmodel_repository.QuotesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class QuotesViewmodel (private val quotesRepository: QuotesRepository): ViewModel(){
    init {
        viewModelScope.launch(Dispatchers.IO) {
            quotesRepository.getQuote()
        }
    }
    val quote:LiveData<Quotes>
    get() = quotesRepository.quotes
}
