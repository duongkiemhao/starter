package com.siliconstack.training.adapter.demo_app.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.databinding.SetupActivityBinding

class SetupActivity : AppCompatActivity() {

    lateinit var binding:SetupActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding = DataBindingUtil.setContentView(this,R.layout.setup_activity)
    }
}