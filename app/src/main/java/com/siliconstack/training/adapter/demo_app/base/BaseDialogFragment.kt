package com.siliconstack.training.adapter.demo_app.base

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import com.siliconstack.training.R

abstract class BaseDialogFragment:DialogFragment() {

    protected abstract fun init()
    protected abstract fun observe()
    protected abstract fun setListener()
    protected abstract fun load()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.DialogSlideAnim)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        observe()
        setListener()
        load()
    }
    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.setCanceledOnTouchOutside(false)
            dialog!!.setCancelable(false)
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog!!.window?.setLayout(width, height)
            dialog!!.window?.setBackgroundDrawable(ResourcesCompat.getDrawable(resources,R.drawable.transparent,null)!!)

        }
    }


}