package com.siliconstack.training.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.siliconstack.training.R
import com.siliconstack.training.databinding.FragmentThirdBinding

class ThirdFragment : Fragment() {
    private lateinit var binding: FragmentThirdBinding
    private lateinit var dialog_fragment:DialogFragment
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(layoutInflater,R.layout.fragment_third,container,false)

        goToDialogFragment()

        getTextFromDialog()

        return binding.root
    }


    private fun goToDialogFragment() {
        dialog_fragment = DialogFragment()
        binding.btnDialogFrag.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.apply {
                replace(R.id.dialog_container,dialog_fragment)
                addToBackStack("fragment_third")
                commit()
            }
        }
    }


    private fun getTextFromDialog() {
        parentFragmentManager.setFragmentResultListener("DataKey",this){ requestKey,bundle ->
            val result_text = bundle.getString("Data")
            binding.tvOutputThrd.text = result_text
        }
    }
}