package com.siliconstack.training.adapter.demo_app.ui.home

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseActivity
import com.siliconstack.training.databinding.HomeActivityBinding

class HomeActivity: BaseActivity(),DrawerListner{

    lateinit var binding:HomeActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.home_activity)
        super.onCreatedViewDone()
    }

    override fun init() {
//        supportFragmentManager.beginTransaction().add(R.id.layout_drawer_view,ProfileFragment.newInstance(),ProfileFragment::class.java.simpleName).
//        addToBackStack(ProfileFragment::class.java.simpleName).commit()
//        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    override fun observe() {
    }

     override fun setListener() {
        binding.profileDrawer.setOnClickListener {
            Log.d("Debug", "Drawer open")
            binding.drawerLayout.openDrawer(Gravity.LEFT)
        }
//        binding.drawerLayout.addDrawerListener(object:DrawerLayout.DrawerListener{
//            override fun onDrawerClosed(drawerView: View) {
//                //profileListener?.onClosed()
//                //(supportFragmentManager.findFragmentById (R.id.layout_fragment) as ProfileFragment).test()
//            }
//
//            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
//
//            }
//
//            override fun onDrawerOpened(drawerView: View) {
//
//            }
//
//            override fun onDrawerStateChanged(newState: Int) {
//
//            }
//        })
    }

    override fun load() {
        
    }

//    override fun load() {
//
//    }

    override fun closeDrawer() {
        binding.drawerLayout.closeDrawer(Gravity.LEFT)
    }
}

interface DrawerListner{
        fun closeDrawer()
}