package com.siliconstack.training.adapter.cloneActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.siliconstack.training.R

class CloneActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clone)
    }
}