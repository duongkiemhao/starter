package com.siliconstack.training.adapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.google.android.material.tabs.TabLayoutMediator
import com.siliconstack.training.R
import com.siliconstack.training.databinding.ActivityIntentBinding


import kotlinx.android.synthetic.main.fragment_second.*

class MainActivity : AppCompatActivity() {
     lateinit var binding: ActivityIntentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_intent)

        setUpTab()

    }

    private fun setUpTab() {
        Log.d("TAG", "setUpTab:called ")
        binding.viewPager.adapter= ViewPagerAdapter(supportFragmentManager,lifecycle)
        binding.viewPager.isUserInputEnabled = false
        binding.viewPager.offscreenPageLimit = 4
        TabLayoutMediator(
            binding.tabLayout, binding.viewPager, false, true
        ) { tab, position ->
            val tabView =
                LayoutInflater.from(this@MainActivity)
                    .inflate(R.layout.tab_home, null) as View
            val titleResId = setTitleNTabIconId(position)
            (tabView.findViewById(R.id.textView_tab) as TextView).text = titleResId[0]
            (tabView.findViewById(R.id.imageView_tab) as ImageView).setImageResource(titleResId[1].toInt())
            tab.customView = tabView
        }.attach()
    }

    private fun setTitleNTabIconId(position: Int): Array<String>{
        when (position) {
            0 -> {
                return arrayOf(getString(R.string.home), R.drawable.homes.toString())
            }
            1 -> {
                return arrayOf(getString( R.string.message), R.drawable.message.toString())

            }
            2 -> {
                return arrayOf(getString(R.string.search), R.drawable.search.toString())
            }
            else -> {
                return arrayOf(getString(R.string.Recyler), R.drawable.ic_recyclerview.toString())
            }

        }
    }
}