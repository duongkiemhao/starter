package com.siliconstack.training.adapter.examples.share_pref

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.setFragmentResult
import com.siliconstack.training.R
import com.siliconstack.training.adapter.base.base.BaseFragment
import com.siliconstack.training.databinding.SharePrefFragmentOneBinding
import com.siliconstack.training.databinding.SharePrefFragmentThreeBinding
import kotlinx.android.synthetic.main.share_pref_fragment_one.*
import kotlinx.android.synthetic.main.share_pref_fragment_two.*

const val MATERIAL = "material"
const val TAG = "Debug"
class SharePrefFragmentOne:BaseFragment() {


    companion object {
        fun newInstance(): SharePrefFragmentOne {
            val fragment = SharePrefFragmentOne()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }
    private var testValue:String? = null
    var values = arrayListOf<switchClass>()
    lateinit var binding: SharePrefFragmentOneBinding
    lateinit var selected: String
    private var getSwitchBoolean: Boolean? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.share_pref_fragment_one, null, false)
        return binding.root
    }

    override fun init() {
        val items = listOf("Material", "Design", "Components", "Android")
        val adapter = ArrayAdapter(requireContext(), R.layout.list_item, items)
        (selection_text_layout.editText as? AutoCompleteTextView)?.setAdapter(adapter)
        values = arrayListOf(switchClass(true),switchClass(false))
        values.add(switchClass(true))
        Log.i(TAG, "load: $values")

        Log.i(TAG, "init: initCalled")
        parentFragmentManager.setFragmentResultListener("requestKey",requireActivity()) { _, bundle ->
            val item=bundle.getString("data")
            Log.i(TAG, "init: recieved")
            Log.i("Debug", "setListener in init:${item.toString()}")
        }

    }

    override fun observe() {

    }

    override fun setListener() {
        binding.btnNext.setOnClickListener {
            parentFragmentManager.beginTransaction()
                .add(R.id.nav_host_share_pref, SharePrefFragmentTwo.newInstance())
                .addToBackStack(SharePrefFragmentTwo::class.simpleName).commit()
        }
        binding.save.setOnClickListener {
            selected = binding.selectionTextLayout.editText?.text.toString()
            Log.i("Debug", "loadText:$selected ")
            PreferenceHelper.putText(requireContext(), MATERIAL, selected)
            binding.selectText.text = selected

//            setFragmentResult(this::class.java.simpleName, bundleOf(TAG to selected))
        }
//        binding.switchSelectedText.setOnClickListener {
//
////            selected = binding.selectionTextLayout.editText?.text.toString()
////            Log.i("Debug", "loadText:$selected ")
////            binding.selectText.text = selected
////            Log.i("Debug", "one:${binding.rememberMe.isChecked} ")
////            var check = binding.rememberMe.isChecked
////            Log.i("Debug", "two:$check ")
////            binding.rememberMe.isChecked =true
////            Log.i("Debug", "after_true:${binding.rememberMe.isChecked} ")
////            binding.rememberMe.isChecked = false
//        }


        binding.switchSelectedText.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                PreferenceHelper.setSwitchToggleBtn(requireContext(), "boolean", true)
                values.add(switchClass(true))
//                Log.i(TAG, "switchOn state saved ${onSwitch.toString()}")
            }
        }

        binding.load.setOnClickListener {
            if (binding.rememberMe.isChecked) {
                loadText()
            } else {
                binding.loadText.text = "text not found"
            }
        }
    }

    private fun loadText() {
        val loadText = PreferenceHelper.getText(requireContext(), MATERIAL)
        binding.loadText.text = loadText.toString()
    }

    override fun load() {
        Log.i(TAG, "nullable value: $testValue ")
        getSwitchBoolean = PreferenceHelper.getSwitchToggleBtn(requireContext(), "boolean")
        Log.i(TAG, "getvalue: $getSwitchBoolean")
        binding.switchSelectedText.isChecked = getSwitchBoolean!!

//        val listOfSwitch = arrayListOf<switchClass>()

    }
    
}

data class switchClass(
    val valueOn:Boolean,
        )